﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharactersApi.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "", "Dominic Toretto", "Male", "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Vin_Diesel_XXX_Return_of_Xander_Cage_premiere.png/800px-Vin_Diesel_XXX_Return_of_Xander_Cage_premiere.png" },
                    { 2, "", "Brian O'Connor", "Male", "https://upload.wikimedia.org/wikipedia/commons/9/91/PaulWalkerEdit-1.jpg" },
                    { 3, "Gandalf", "Gandalf", "Male", "https://static.feber.se/article_images/31/44/88/314488_980.gif" },
                    { 4, "", "Frodo Bagger", "Male", "https://i.pinimg.com/originals/63/77/c6/6377c6c26401f23739439440d67957f4.jpg" },
                    { 5, "", "Sam Gamgi", "Male", "https://i.pinimg.com/736x/ec/a6/2a/eca62aea2ab4b143ed58e5285b57dfa9--samwise-gamgee-hair-pulled-back.jpg" },
                    { 6, "Sméagol", "Gollum", "Male", "https://imgs.aftonbladet-cdn.se/v2/images/72739752-fe8b-4ec1-95e0-215cf8accbc3?fit=crop&h=1132&q=50&w=800&s=bd3e449b3b3e7431845ddd9f2c3d640499c21678" },
                    { 7, "Batman", "Bruce Wayne", "Male", "https://upload.wikimedia.org/wikipedia/en/1/19/Bruce_Wayne_%28The_Dark_Knight_Trilogy%29.jpg" },
                    { 8, "Red hood", "Joker", "Male", "https://static.wikia.nocookie.net/batman/images/f/f9/Heath_Ledger_as_the_Joker.JPG/revision/latest/top-crop/width/360/height/450?cb=20090903145508" },
                    { 9, "Bane", "Unknown", "Male", "https://static.posters.cz/image/1300/plakater/batman-dark-knight-rises-bane-sewer-i12721.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Fast & Furious (also known as The Fast and the Furious) is a media franchise centered on a series of action films that are largely concerned with illegal street racing, heists, and spies. The franchise also includes short films, a television series, live shows, video games, and theme park attractions. It is distributed by Universal Pictures.", "The Fast and Furious" },
                    { 2, "Batman Begins is a reboot of the entire Batman film franchise under the direction of Christopher Nolan. It revolves around Bruce Wayne journey into becoming The Dark Knight of Gotham City from crime and corruption . In his quest he encounters allies that aid him in his quest such as his loyal butler and confident Alfred, local police officer and friend Jim Gordon, fellow gadgetry and tech equipment specialist Lucius Fox and finally childhood friend Rachel Dawes. With the rises of Batman came enemies to challenge him such as Joker and the Scarecrow.", "Batman series" },
                    { 3, "The Lord of the Rings is the saga of a group of sometimes reluctant heroes who set forth to save their world from consummate evil. Its many worlds and creatures were drawn from Tolkien’s extensive knowledge of philology and folklore.", "The lord of the rings" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "PictureUrl", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { 2, "John Singleton", null, "Action", "https://upload.wikimedia.org/wikipedia/en/9/9d/Two_fast_two_furious_ver5.jpg", "2003", "2 Fast 2 Furious", "https://www.youtube.com/watch?v=F_VIM03DXWI" });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharactersId", "MoviesId" },
                values: new object[,]
                {
                    { 1, 2 },
                    { 2, 2 }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "PictureUrl", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Rob Cohen", 1, "Action", "https://upload.wikimedia.org/wikipedia/en/5/54/Fast_and_the_furious_poster.jpg", "2001", "Fast & Furious", "https://www.youtube.com/watch?v=2TAOizOnNPo" },
                    { 3, "Justin Lin", 1, "Action", "https://upload.wikimedia.org/wikipedia/en/4/4f/Poster_-_Fast_and_Furious_Tokyo_Drift.jpg", "2006", "The Fast and the Furious: Tokyo Drift", "https://www.youtube.com/watch?v=p8HQ2JLlc4E" },
                    { 4, "Christopher Nolen", 2, "Action", "https://m.media-amazon.com/images/M/MV5BOTY4YjI2N2MtYmFlMC00ZjcyLTg3YjEtMDQyM2ZjYzQ5YWFkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg", "2005", "Batman Begins", "https://www.youtube.com/watch?v=neY2xVmOfUM" },
                    { 5, "Christopher Nolen", 2, "Action", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOH1Z3WEv-NS_ahc7NSQRz1LTh813Z6sMmI_E7d1_FYpQd0MxY", "2008", "The Dark night", "https://www.youtube.com/watch?v=EXeTwQWrcwY" },
                    { 6, "Christopher Nolen", 2, "Action", "https://m.media-amazon.com/images/M/MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@._V1_FMjpg_UX1000_.jpg", "2012", "The Dark Knight Rises", "https://www.youtube.com/watch?v=g8evyE9TuYk" },
                    { 7, "Peter Jackson", 3, "Fantasy/Adventure", "https://d2iltjk184xms5.cloudfront.net/uploads/photo/file/127849/small_original.jpeg", "2001", "The Lord of the Rings: The Fellowship of the Ring", "https://www.youtube.com/watch?v=V75dMMIW2B4" },
                    { 8, "Peter Jackson", 3, "Fantasy/Adventure", "https://cdn.cdon.com/media-dynamic/images/product/movie/dvd/image2/lord_of_the_rings_-_the_two_towers_theatrical_cut_nordic-43043783-.jpg?impolicy=product&w=1280&h=720", "2002", "The Lord of the Rings: The Two Towers", "https://www.youtube.com/watch?v=LbfMDwc4azU" },
                    { 9, "Peter Jackson", 3, "Fantasy/Adventure", "https://upload.wikimedia.org/wikipedia/en/b/be/The_Lord_of_the_Rings_-_The_Return_of_the_King_%282003%29.jpg", "2003", "The Lord of the Rings: The Return of the King", "https://www.youtube.com/watch?v=y2rYRu8UW8M" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharactersId", "MoviesId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 4, 9 },
                    { 3, 9 },
                    { 6, 8 },
                    { 5, 8 },
                    { 4, 8 },
                    { 3, 8 },
                    { 6, 7 },
                    { 5, 7 },
                    { 5, 9 },
                    { 4, 7 },
                    { 9, 6 },
                    { 7, 6 },
                    { 8, 5 },
                    { 7, 5 },
                    { 7, 4 },
                    { 2, 3 },
                    { 1, 3 },
                    { 2, 1 },
                    { 3, 7 },
                    { 6, 9 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 3, 7 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 3, 8 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 3, 9 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 7 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 8 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 9 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 5, 7 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 5, 8 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 5, 9 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 6, 7 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 6, 8 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 6, 9 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 7, 4 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 7, 5 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 7, 6 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 8, 5 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 9, 6 });

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
