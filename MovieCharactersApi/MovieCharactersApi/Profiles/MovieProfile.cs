﻿using AutoMapper;
using MovieCharacters.Model;
using MovieCharactersApi.Models.DTOs.MovieDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>().ReverseMap();
            CreateMap<Movie, MovieEditDTO>().ReverseMap();
            CreateMap<Movie, MovieCreateDTO>().ReverseMap();
        }
    }
}
