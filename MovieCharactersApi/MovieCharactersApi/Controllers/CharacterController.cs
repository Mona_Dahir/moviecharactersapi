﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacters.Model;
using MovieCharactersApi.Models.DTOs.CharacterDTO;
using MovieCharactersApi.Service.IService;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharactersApi.Controllers
{
    [ApiController]
    [Route("characters")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharacterController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        /// Retrieve all characters from database.
        /// </summary>
        /// <remarks>Returns empty list if no characters exist</remarks>
        /// <returns>List of Characters</returns>
        [SwaggerResponse(200, "Retrieved all Characters")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return Ok(_mapper.Map<IEnumerable<CharacterReadDTO>>(await _characterService.GetAllAsync()));
        }

        /// <summary>
        /// Add a character to database.
        /// </summary>
        /// <remarks>
        /// - FullName max length 100 characters.
        /// - Alias max length 100 characters.
        /// - Gender max length 20 characters.
        /// </remarks>
        /// <param name="character">character to add</param>
        /// <returns>The character added</returns>
        /// <exception cref="DBConcurrencyException">Could not add character to database. Returns 409 Conflict</exception>
        /// <exception cref="DbUpdateException">Invalid string length. Check string length requirements! Returns 400 Bad Request</exception>
        [SwaggerResponse(201, "Character created")]
        [SwaggerResponse(400, "Invalid string length. Check string length requirements!")]
        [SwaggerResponse(409, "Could not add character to database!")]
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter([FromBody] CharacterCreateDTO character)
        {
            var domainCharacter = _mapper.Map<Character>(character);

            try
            {
                domainCharacter = await _characterService.AddAsync(domainCharacter);

                return CreatedAtAction("GetCharacter",
                    new { id = domainCharacter.Id },
                    _mapper.Map<CharacterReadDTO>(domainCharacter));
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }
        }

        /// <summary>
        /// Get a specific character from database.
        /// </summary>
        /// <param name="id">Id of character to retrieve</param>
        /// <returns>The character</returns>
        [SwaggerResponse(200, "Found the character")]
        [SwaggerResponse(404, "Could not find a character matching the id provided")]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _characterService.GetSpecificAsync(id);
            if (character == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<CharacterReadDTO>(character));
        }

        /// <summary>
        /// Update a character
        /// </summary>
        /// <remarks>
        /// - FullName max length 100 characters.
        /// - Alias max length 100 characters.
        /// - Gender max length 20 characters.
        /// </remarks>
        /// <param name="id">Character id</param>
        /// <param name="character">Character with fields updated</param>
        /// <returns></returns>
        /// <exception cref="DBConcurrencyException">Could not add character to database. Returns 409 Conflict</exception>
        /// <exception cref="DbUpdateException">Invalid string length. Check string length requirements! Returns 400 Bad Request</exception>
        [SwaggerResponse(204, "The character was updated")]
        [SwaggerResponse(400, "Check string length requirements!")]
        [SwaggerResponse(404, "The character does not exist")]
        [SwaggerResponse(409, "Could not update the character!")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO character)
        {
            if (!_characterService.Exists(id))
            {
                return NotFound();
            }

            var domainCharacter = _mapper.Map<Character>(character);
            domainCharacter.Id = id;
            try
            {
                await _characterService.UpdateAsync(domainCharacter);
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Remove a character
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <returns></returns>
        /// <exception cref="DBConcurrencyException">Could not delete the character from database. Returns 409 Conflict</exception>
        [SwaggerResponse(204, "The character was removed")]
        [SwaggerResponse(404, "The character could not be found")]
        [SwaggerResponse(409, "Could not delete the character!")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.Exists(id))
            {
                return NotFound();
            }
            try
            {
                await _characterService.DeleteAsync(id);
            }
            catch
            {
                Conflict();
            }
            return NoContent();
        }
    }
}
